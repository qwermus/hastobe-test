<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Get Finish Helper
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 */
trait GetFinishHelper
{
    /**
     * Find when status will finish
     *
     * @param object $plan
     * @param array $workingHours
     * @param int $timestamp
     * @return int
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function getFinish(object $plan, ?object $workingHours, int $timestamp) : ?int
    {
        // If station is working 24/7 - we do not have finish timestamp
        if (app()->make($plan->interval)::isNeverFinish($plan->plan)) {
            return null;
        }

        // If station is working - we need to get when it will finish
        if (!is_null($workingHours)) {
            return app()->make($plan->interval)::convertTimeToTimestamp($workingHours->f, $timestamp);
        }

        // If station is not working - we need to find next working hours start
        return app()->make($plan->interval)::getNextStart($plan->plan, $timestamp);
    }
}
