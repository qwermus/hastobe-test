<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Find Plan Inside Interval Helper
 * Search one interval inside another interval
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 * @date 23.11.2020
 */
trait FindPlanInsideIntervalHelper
{
    /**
     * We need to find overlap between our working plan and time period
     *
     * @param object $plan
     * @param int $start
     * @param int $finish
     * @return object|null
     */
    private function findPlanInsideInterval(object $plan, int $start, ?int $finish) : ?int
    {
        // Iterate working hours and find overlap
        foreach ($plan->plan as $workingHours) {

            // Find if there is an overlap and return current working hours
            if (app()->make($plan->interval)->isInside($workingHours->s, $workingHours->f, $start, $finish)) {
                $finish = app()->make($plan->interval)::convertTimeToTimestamp($workingHours->s, $start);
            }

        }
        // If overlap was not found - return null
        return $finish;
    }
}
