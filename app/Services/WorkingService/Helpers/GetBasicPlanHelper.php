<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Get Basic Plan Helper
 * Here we get basic plan for our station
 * Basic plan always must exists and can be only one on each level
 * If you want to use several basic plans on one level - you need to merge them first
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 * @date 23.11.2020
 */
trait GetBasicPlanHelper
{
    /**
     * Get basic plan - iterate all instances by priority. First founded plan will be our basic plan
     *
     * @param object $plan
     * @param int $timestamp
     * @return bool
     */
    private function getBasicPlan(array $instances) : ?object
    {
        // Iterate instances [ChargingStation, Store, Tenant]
        foreach ($instances as $instance) {

            if (!is_null($plan = $this->getWorkplans($instance, 'Basic'))) {
                return $plan->first();
            }
        }

        return null;
    }
}
