<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Find Overlap Helper
 * Here we compare timestamp with interval to find whether timestamp is inside interval
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 * @date 23.11.2020
 */
trait FindLowerLeveledPlanHelper
{
    /**
     * We need to find lower leveled plan inside interval, which has more priority
     *
     * @param array $instances
     * @param int $instanceKey
     * @param string $status
     * @param int $timestamp
     * @param int $finish
     * @return int
     */
    private function findLowerLeveledPlan(array $instances, ?int $instanceKey, string $status, int $timestamp, ?int $finish) : ?int
    {
        // We have a time interval between start ($timestamp) and finish ($finish)
        // We need to search all LOWER instances to find something in this interval
        // If there is lower plan - it has priority
        foreach ($instances as $key => $instance) {

            // Break if it is not lower instance
            if ($key === $instanceKey) {
                break;
            }

            // It there are no plans on current level - go next instance
            if (is_null($plans = $this->getWorkplans($instance, 'Exception'))) {
                continue;
            }

            // Iterate plans on one level
            foreach ($plans as $plan) {

                // If statuses are the same - it is not interesting for us, go next
                if ($plan->status == $status) {
                    continue;
                }

                // Let's find overlap of two periods. If overlap exists - move finish variable backward
                $finish = $this->findPlanInsideInterval($plan, $timestamp, $finish);

            }
        }

        // When we finished iteration - return finish variable.
        // If nothing was found - variable did not changed
        return $finish;
    }
}
