<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Find Next Status In Exceptions Helper
 * First step we need to get if current status is in exception - it has priority
 * If there is an exception - we can be sure, that while it is active, status will not change.
 * Each founded exception will move our timestamp forward and we will search status from the next moment
 * Lo, lets find exception
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 * @date 23.11.2020
 */
trait FindNextStatusInExceptionsHelper
{
    /**
     * Find Next Status In Exceptions
     *
     * @param array $instances
     * @param int $timestamp
     * @return int
     */
    private function findNextStatusInExceptions(array $instances, int $timestamp) : int
    {
        //  Iterate all instances - first priority has charging station, last priority has tenant
        foreach ($instances as $instanceKey => $instance) {

            // If this instance has no plans - go next instance
            if (is_null($plans = $this->getWorkplans($instance, 'Exception'))) {
                continue;
            }

            // Iterate all exception plans on current level...
            foreach ($plans as $plan) {

                // ...and find overlap. If overlap doesn't exists - this pla is not active, go next plan
                if (is_null($workingHours = $this->findOverlap($plan, $timestamp))) {
                    continue;
                }

                // Otherwise we also need to check exceptions on lower levels inside this - they have priority
                // For example, now store is working, but after one minute charging station will be closed for construction
                return $this->findLowerLeveledPlan($instances, $instanceKey, $plan->status, $timestamp, $workingHours->f);
            }
        }

        // If nothing was found - there are no exceptions at current moment, return timestamp
        return $timestamp;
    }
}
