<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Find Current Status Helper
 * We search status by timestamp
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 * @date 23.11.2020
 */
trait FindCurrentStatusHelper
{
    /**
     * Find current status inside one level instance
     *
     * @param object $instance
     * @param string $type
     * @param int $timestamp
     * @return string|null
     */
    private function findCurrentStatus(object $instance, string $type, int $timestamp) : ?string
    {
        // Get working plans of the current instance. Continue if they are empty
        // We can use eloquent every time because laravel do not send query to database each time, it use cache
        // So we can do this as much as we want
        // Otherwise, we need to get all plans and save them to class property or variable
        if (is_null($plans = $this->getWorkplans($instance, $type))) {
            return null;
        }

        // Iterate plans on one level
        foreach ($plans as $plan) {

            // And find overlap. If overlap exists - return status as boolean
            if (!is_null($workingHours = $this->findOverlap($plan, $timestamp))) {
                return $plan->status;
            }
        }

        return null;
    }
}
