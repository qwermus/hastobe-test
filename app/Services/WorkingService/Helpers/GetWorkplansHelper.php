<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Get Workplans Helper
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 * @date 23.11.2020
 */
trait GetWorkplansHelper
{
    private static function getWorkplans(object $instance, string $type) : ?object
    {
        return $instance->workplans()->whereType($type)->get();
    }
}
