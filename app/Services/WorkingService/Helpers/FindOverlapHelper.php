<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Find Overlap Helper
 * Here we compare timestamp with interval to find whether timestamp is inside interval
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 * @date 23.11.2020
 */
trait FindOverlapHelper
{
    /**
     * We need to find overlap between our timestamp and plan.
     * If there is overlap - return true
     *
     * @param object $plan
     * @param int $timestamp
     * @return bool
     */
    private function findOverlap(object $plan, int $timestamp) : ?object
    {
        // If something wrong and object is empty - go next
        if (is_null($plan)) {
            return null;
        }

        // Iterate working hours and find overlap
        foreach ($plan->plan as $workingHours) {

            // Find if there is an overlap and return current status
            if (app()->make($plan->interval)->isOverlaped($workingHours->s, $workingHours->f, $timestamp)) {
                return $workingHours;
            }
        }
        // If nothing found - return null
        return null;
    }
}
