<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Get current status
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 */
trait GetCurrentStatusHelper
{
    /**
     * Return charging station status
     *
     * @param string $status
     * @param array $workingHours
     * @return string
     */
    private function getCurrentStatus(string $status, ?object $workingHours) : string
    {
        // If there are working hours - we need to return status
        if (!is_null($workingHours)) {
            return $status;
        }
        // Otherwise return closed status
        return app()->make('Closed')->statusString();
    }
}
