<?php

namespace App\Services\WorkingService\Helpers;

/**
 * Find Next Status In Exceptions Helper
 * First step we need to get if current status is in exception - it has priority
 * If there is an exception - we can be sure, that while it is active, status will not change.
 * Each founded exception will move our timestamp forward and we will search status from the next moment
 * Lo, lets find exception
 *
 * @package App\Services\WorkingService\Helpers
 * @author Volodymyr Lvov
 * @date 23.11.2020
 */
trait FindNextStatusInBasicHelper
{
    /**
     * Find Next Status In Basic
     *
     * @param array $instances
     * @param int $timestamp
     * @return int
     */
    private function findNextStatusInBasic(array $instances, int $timestamp, object $basic, bool $isOpen) : ?int
    {
        /**
         * If we didn't find overlap in exceptions, we need to find overlap in basic plan
         */
        $workingHours = $this->findOverlap($basic, $timestamp);

        /**
         * If status changed - that's it, we found what we were looking for
         * Return timestamp
         */
        if (
            (is_null($workingHours) && $isOpen === true) || // If status changed from open to closed...
            (!is_null($workingHours) && $isOpen === false) // ...or from closed to open
        ) {
            return $timestamp;
        }

        // Now, when we have new interval, we can search for exceptions inside it
        // If we found an exception inside period - move timestamp forward
        return $this->findLowerLeveledPlan(
            $instances,
            null,
            $this->getCurrentStatus($basic->status, $workingHours),
            $timestamp,
            $this->getFinish($basic, $workingHours, $timestamp)
        );
    }
}
