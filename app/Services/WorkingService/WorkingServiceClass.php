<?php

namespace App\Services\WorkingService;

/**
 * Helpers
 */

use App\Services\WorkingService\Helpers\GetFinishHelper;
use App\Services\WorkingService\Helpers\FindOverlapHelper;
use App\Services\WorkingService\Helpers\GetBasicPlanHelper;
use App\Services\WorkingService\Helpers\GetWorkplansHelper;
use App\Services\WorkingService\Helpers\GetCurrentStatusHelper;
use App\Services\WorkingService\Helpers\FindCurrentStatusHelper;
use App\Services\WorkingService\Helpers\FindLowerLeveledPlanHelper;
use App\Services\WorkingService\Helpers\FindNextStatusInBasicHelper;
use App\Services\WorkingService\Helpers\FindPlanInsideIntervalHelper;
use App\Services\WorkingService\Helpers\FindNextStatusInExceptionsHelper;


/**
 * Class Working Service Interface
 * @package App\Services\WorkingService
 * @author Volodymyr Lvov
 */
class WorkingServiceClass implements WorkingServiceInterface
{
    /**
     * Add helpers traits
     */
    use GetFinishHelper;
    use FindOverlapHelper;
    use GetBasicPlanHelper;
    use GetWorkplansHelper;
    use GetCurrentStatusHelper;
    use FindCurrentStatusHelper;
    use FindLowerLeveledPlanHelper;
    use FindNextStatusInBasicHelper;
    use FindPlanInsideIntervalHelper;
    use FindNextStatusInExceptionsHelper;

    /**
     * Calculate whether current charging station is working now
     * We can easy change this method to work with stores and tenants also.
     * We just need to define correct variable $parents (to store or tenant)
     *
     * @param array $instances [working plans array]
     * @param int $timestamp [timestamp to find something by it]
     * @return bool
     */
    public function isOpen(array $instances, int $timestamp) : bool
    {
        // First we need to iterate instances [ChargingStation, Store, Tenant] and find first exception
        foreach ($instances as $instance) {
            // If overlap was found - it is our status, return it
            if (!is_null($status = $this->findCurrentStatus($instance, 'Exception', $timestamp))) {
                return app()->make($status)::statusBool();
            }
        }

        // If there is no exception - let search in basic plan
        $basic = $this->getBasicPlan($instances);

        // And find overlap. If overlap exists - return status as boolean
        if (!is_null($this->findOverlap($basic, $timestamp))) {
            return app()->make($basic->status)::statusBool();
        }

        // If nothing found - return false, station is closed
        return false;
    }

    /**
     * Here we need to calculate when next status begins
     *
     * @param array $instances
     * @param int $timestamp
     * @return int|null
     */
    public function nextStatus(array $instances, int $timestamp) : ?int
    {
        // Current isOpen status
        // It will be defined in first iteration
        $isOpen = null;

        // Save starting value - it will be used as stop factor
        // In real life we do not need a stop factor because all conflicts will be check while creating a plan
        // But now we don't check conflicts, so it is better to create a stop-factor
        $stop = $timestamp + (config('hastobe.exception_max_length', 31536000));

        // Get basic plan to work with it later
        $basic = $this->getBasicPlan($instances);

        // Start while-cycle
        // We will check status at current moment, and if it did not changed,
        // we will rewind time forward until we come across another status.
        // In real life in most cases it will be only one iteration, sometime maximum 3 iterations
        while (true) {
            /**
             * First we need to get isOpen status at current moment
             */
            $isOpenNow = $this->isOpen($instances, $timestamp);

            // And save it
            if (is_null($isOpen)) {
                $isOpen = $isOpenNow;
            }

            // If at current moment status is different from the beginning status - we are done
            if ($isOpen != $isOpenNow) {
                return $timestamp;
            }

            // Also for testing task we need a stop factor, for example, two years
            // It is long termine and we don't know, what will be after two years
            if ($timestamp >= $stop) {
                return null;
            }

            // Make a copy of the timestamp to compare it with getted values
            $timestampReference = $timestamp;

            /**
             * First step is to find current status in exceptions
             * If current status is exception - it means that status will not change until it finished
             */
            $timestamp = $this->findNextStatusInExceptions($instances, $timestamp);

            // If it was changed - go to beginning
            if ($timestampReference != $timestamp) {
                continue;
            }

            /**
             * Second step is to find current status in basic plan.
             * But we need to know that basic plan has no priority
             */
            $timestamp = $this->findNextStatusInBasic($instances, $timestamp, $basic, $isOpen);

            // If timestamp was changed - go to beginning
            if (!is_null($timestamp) && $timestampReference != $timestamp) {
                continue;
            }

            // Otherwise, if there are no exceptions - we did it. Congratulations!
            return $timestamp;
        }
    }
}
