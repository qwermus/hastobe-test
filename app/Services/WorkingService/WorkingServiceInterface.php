<?php

namespace App\Services\WorkingService;

use App\Models\ChargingStation;

/**
 * Interface Working Service Interface
 * @package App\Services\WorkingService
 * @author Volodymyr Lvov
 */
interface WorkingServiceInterface
{
    /**
     * Compare two dates to find if station is working at current moment
     *
     * @param array $instances [working plans array]
     * @param int $timestamp [timestamp to find something by it]
     * @return bool
     */
    public function isOpen(array $instances, int $timestamp) : bool;

    /**
     * Here we need to calculate when next status begins
     *
     * @param array $instances
     * @param int $timestamp
     * @return int|null
     */
    public function nextStatus(array $instances, int $timestamp) : ?int;
}
