<?php

namespace App\Services\Status;

/**
 * Class Status Open Class
 * @package App\Services\Status
 * @author Volodymyr Lvov
 */
class StatusOpenClass implements StatusInterface
{
    /**
     * Return current status in boolean format - true means open
     *
     * @return bool
     */
    public static function statusBool() : bool
    {
        return true;
    }

    /**
     * Return current status in string format
     *
     * @return bool
     */
    public static function statusString() : string
    {
        return 'Open';
    }
}
