<?php

namespace App\Services\Status;

/**
 * Class Status Closed Class
 * @package App\Services\Status
 * @author Volodymyr Lvov
 */
class StatusClosedClass implements StatusInterface
{
    /**
     * Return current status in boolean format - false means closed
     *
     * @return bool
     */
    public static function statusBool() : bool
    {
        return false;
    }

    /**
     * Return current status in string format
     *
     * @return bool
     */
    public static function statusString() : string
    {
        return 'Closed';
    }
}
