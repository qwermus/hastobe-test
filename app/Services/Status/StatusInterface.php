<?php

namespace App\Services\Status;

/**
 * Interface Status Interface
 * @package App\Services\Status
 * @author Volodymyr Lvov
 */
interface StatusInterface
{
    /**
     * Return current status in boolean format
     *
     * @return bool
     */
    public static function statusBool() : bool;

    /**
     * Return current status in string format
     *
     * @return bool
     */
    public static function statusString() : string;
}
