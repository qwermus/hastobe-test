<?php

namespace App\Services\Interval;

/**
 * Class Interval Weekday Class
 * @package App\Services\Interval
 * @author Volodymyr Lvov
 */
class IntervalWeekdayClass implements IntervalInterface
{
    /**
     * Compare two dates to find if station is in at current moment
     *
     * @param int $from
     * @param int $till
     * @param int $timestamp
     * @return bool
     */
    public static function isOverlaped(int $from, int $till, int $timestamp) : bool
    {
        // We use seconds from the week beginning. So, we need to prepare our date
        $time = $timestamp - weekStart($timestamp);

        if ($from <= $time && $till > $time) {
            return true;
        }
        return false;
    }

    /**
     * Compare two time intervals to find whether one is inside another
    // We do not use this method at current moment.
     *
     * @param int $from
     * @param int $till
     * @param int $start
     * @param int $finish
     * @return bool
     */
    public static function isInside(int $from, int $till, int $start, int $finish) : bool
    {
        // We do not use this method at current moment.
        return false;
    }

    /**
     * Get when next status will start
     *
     * @param object $plans
     * @param int $timestamp
     * @return int|null
     */
    public static function getNextStart(array $plans, int $timestamp) : ?int
    {
        // Get week start
        $weekstart = weekStart($timestamp);

        // Find closest next start
        $closest = self::findClosest($plans, $timestamp - $weekstart);
        if (!is_null($closest->nextstart)) {
            return $closest->nextstart + $weekstart;
        }

        // If we did not find anything - it means the week is over. Lets iterate next week
        $closest = self::findClosest($plans, -1);
        if (!is_null($closest->nextstart)) {
            return $closest->nextstart + $weekstart + (60*60*24*7);
        }

        // Otherwise return null
        return null;
    }

    /**
     * Check if current plan will newer finish.
     * Available only for weekday plans.
     *
     * @param array $plans
     * @return bool
     */
    public static function isNeverFinish(array $plans) : bool
    {
        // Pay attention: plans cannot be close each other. If they are close, they must be united while creation.
        foreach ($plans as $plan) {
            if ($plan->s <= 0 && $plan->f >= 60*60*24*7) {
                return true;
            }
        }
        return false;
    }

    /**
     * Find closest statuses
     *
     * @param object $plans
     * @param int $time
     * @return object
     */
    private static function findClosest(array $plans, int $time) : object
    {
        $return = (object)[
            'nextstart' => null, // When next status will start
            'prevfinish' => null, // When previous status finished
        ];

        // Iterate plans to get values and find closest
        // Pay attention: plans cannot touch each other. If they touch, they must be united while creation.
        foreach ($plans as $plan) {

            // When next status will start
            if ($plan->s > $time) {
                $return->nextstart = min($return->nextstart, $plan->s) ?? $plan->s;
            }

            // // When previous status finished
            if ($plan->f <= $time) {
                $return->prevfinish = min($return->prevfinish, $plan->f) ?? $plan->f;
            }
        }

        return $return;
    }

    /**
     * Convert working hours to timestamp
     * In different "intervals" time can be set it different way.
     * In weekdays we have working hours as time from week beginning
     *
     * @param int $time
     * @param int $timastamp
     * @return int
     */
    public static function convertTimeToTimestamp(int $time, int $timestamp) : int
    {
        return weekStart($timestamp) + $time;
    }
}
