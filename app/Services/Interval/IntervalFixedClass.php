<?php

namespace App\Services\Interval;

/**
 * Class Interval Fixed Class
 * @package App\Services\Interval
 * @author Volodymyr Lvov
 */
class IntervalFixedClass implements IntervalInterface
{
    /**
     * Compare two dates to find if station is in at current moment
     *
     * @param int $from
     * @param int $till
     * @param int $timestamp
     * @return bool
     */
    public static function isOverlaped(int $from, int $till, int $timestamp) : bool
    {
        if ($from <= $timestamp && $till > $timestamp) {
            return true;
        }
        return false;
    }

    /**
     * Compare two time intervals to find whether one is inside another
     *
     * @param int $from
     * @param int $till
     * @param int $start
     * @param int $finish
     * @return bool
     */
    public static function isInside(int $from, int $till, int $start, ?int $finish) : bool
    {
        // If plan is in past
        if ($till <= $start) {
            return false;
        }

        // In plan is in future
        if (!is_null($finish) && $from >= $finish) {
            return false;
        }

        return true;
    }


    /**
     * Get when next status will start
    // We do not use this method at current moment.
     *
     * @param object $plans
     * @param int $timestamp
     * @return int|null
     */
    public static function getNextStart(array $plans, int $timestamp) : ?int
    {
        // We do not use this method at current moment.
        return false;
    }

    /**
     * In different "intervals" time can be set it different way.
     * In fixed time is a timestamp yet. So, just return time
     *
     * @param int $time
     * @param int $timastamp
     * @return int
     */
    public static function convertTimeToTimestamp(int $time, int $timestamp) : int
    {
        return $time;
    }

    /**
     * Check if current plan will newer finish. For fixed plan it is always false
     *
     * @param array $plans
     * @return bool
     */
    public static function isNeverFinish(array $plans) : bool
    {
        return false;
    }
}
