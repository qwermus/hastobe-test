<?php

namespace App\Services\Interval;

/**
 * Interface Interval Interface
 * @package App\Services\Interval
 * @author Volodymyr Lvov
 */
interface IntervalInterface
{
    /**
     * Compare two dates to find if station is in at current moment
     *
     * @param int $from
     * @param int $till
     * @param int $timestamp
     * @return bool
     */
    public static function isOverlaped(int $from, int $till, int $timestamp) : ?bool;

    /**
     * Compare two time intervals to find whether one is inside another
     *
     * @param int $from
     * @param int $till
     * @param int $start
     * @param int $finish
     * @return bool
     */
    public static function isInside(int $from, int $till, int $start, int $finish) : bool;

    /**
     * Get when next status will start
     *
     * @param object $plans
     * @param int $timestamp
     * @return int|null
     */
    public static function getNextStart(array $plans, int $timestamp) : ?int;

    /**
     * In different "intervals" time can be set it different way.
     * So, we need to convert time from the interval-format to unix timestamp
     *
     * @param int $time
     * @param int $timastamp
     * @return int
     */
    public static function convertTimeToTimestamp(int $time, int $timestamp) : int;

    /**
     * Check if current plan will newer finish. For fixed plan it is always false
     *
     * @param array $plans
     * @return bool
     */
    public static function isNeverFinish(array $plans) : bool;
}
