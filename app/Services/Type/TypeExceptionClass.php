<?php

namespace App\Services\Type;

/**
 * Class Type Exception Class
 * @package App\Services\Type
 * @author Volodymyr Lvov
 */
class TypeExceptionClass implements TypeInterface
{
}
