<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\WorkingService\WorkingServiceClass;

use App\Services\Status\StatusOpenClass;
use App\Services\Status\StatusClosedClass;

use App\Services\Interval\IntervalFixedClass;
use App\Services\Interval\IntervalWeekdayClass;

use App\Services\Type\TypeBasicClass;
use App\Services\Type\TypeExceptionClass;

/**
 * Class SingletonServiceProvider
 * @package App\Providers
 */
class SingletonServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Of course, it is better to use prefixes here,
     * But now we do not need them
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Working service
         * Of course, it is better to use prefixes here,
         * But now we do not need them
         */
        $this->app->singleton('WorkingService', function ($app) {
            return new WorkingServiceClass();
        });

        /**
         * Statuses
         */
        $this->app->singleton('Open', function ($app) {
            return new StatusOpenClass();
        });
        $this->app->singleton('Closed', function ($app) {
            return new StatusClosedClass();
        });

        /**
         * Intervals
         */
        $this->app->singleton('Fixed', function ($app) {
            return new IntervalFixedClass();
        });
        $this->app->singleton('Weekday', function ($app) {
            return new IntervalWeekdayClass();
        });

        /**
         * Types
         */
        $this->app->singleton('Basic', function ($app) {
            return new TypeBasicClass();
        });
        $this->app->singleton('Exception', function ($app) {
            return new TypeExceptionClass();
        });
    }
}
