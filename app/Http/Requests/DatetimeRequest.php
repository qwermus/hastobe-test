<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Datetime Request
 * @package App\Http\Requests
 * @author Volodymyr Lvov
 */
class DatetimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // Get tag from path
        $this->merge([
            'datetime' => $this->route('datetime'),
            'timestamp' => strtotime(
                $this->route('datetime')
            )
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'datetime'=> ['required', 'date']
        ];
    }
}
