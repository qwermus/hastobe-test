<?php

namespace App\Http\Controllers;

use App\Models\ChargingStation;
use App\Http\Requests\DatetimeRequest;

/**
 * Class Is Working Controller
 * @package App\Http\Controllers
 * @author Volodymyr Lvov
 */
class IsWorkingController extends Controller
{
    /**
     * We get charging station and date and return whether it open or closed
     *
     * @param DatetimeRequest $request
     * @param ChargingStation $chargingStation
     */
    public function index(DatetimeRequest $request, ChargingStation $chargingStation) : array
    {
        // Get current status as bool
        $isOpen = $chargingStation->isOpen($request->timestamp);

        // Get next status timestamp
        $nextStatus = $chargingStation->nextStatus($request->timestamp);

        // Return information
        return [
            'current_status_name'  => $isOpen ? 'Open' : 'Closed',
            'next_status_start' => $nextStatus ? date('Y-m-d H:i:s', $nextStatus) : 'Never'
        ];
    }
}
