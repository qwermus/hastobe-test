<?php

/**
 * Get week start timestamp
 */
if (!function_exists("weekStart")) {
    function weekStart(int $datetime) : int {
        // Get week and day parameters
        $day = date('N', $datetime);
        $hour = date('H', $datetime);
        $minute = date('i', $datetime);
        $second = date('s', $datetime);

        // Calculate time from the week beginning
        return $datetime - (60*60*24 * ($day-1)) - (60*60*$hour) - (60*$minute) - $second;
    }
}


/**
 * Get week finish timestamp
 */
if (!function_exists("weekFinish")) {
    function weekFinish(int $datetime) : int {
        return weekStart($datetime) + (60*60*24*7);
    }
}
