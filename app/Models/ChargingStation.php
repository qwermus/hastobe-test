<?php

namespace App\Models;

use App\Traits\PlanableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ChargingStation
 * @package App\Models
 * @author Volodymyr Lvov
 */
class ChargingStation extends Model
{
    use HasFactory;
    use PlanableTrait;

    /**
     * @var string [Name to use in Planable Trait]
     */
    private $planType = 'ChargingStation';

    /**
     * Station belongs to store
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    /**
     * Get whether charging station is working now or not
     *
     * @param int $timestamp
     * @return bool
     */
    public function isOpen(int $timestamp) : bool
    {
        return app()->make('WorkingService')->isOpen([
            $this,
            $this->store,
            $this->store->tenant
        ], $timestamp);
    }

    /**
     * Get next status switch timestamp
     *
     * @param int $timestamp
     * @return bool
     */
    public function nextStatus(int $timestamp) : ?int
    {
        return app()->make('WorkingService')->nextStatus(
            [
                $this,
                $this->store,
                $this->store->tenant
            ],
            $timestamp
        );
    }


}
