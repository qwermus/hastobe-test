<?php

namespace App\Models;

use App\Traits\PlanableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Store
 * @package App\Models
 * @author Volodymyr Lvov
 */
class Store extends Model
{
    use HasFactory;
    use PlanableTrait;

    /**
     * @var string [Name to use in Planable Trait]
     */
    private $planType = 'Store';

    /**
     * Store belongs to tenant
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }

    /**
     * Store has many charging stations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function chargingStations()
    {
        return $this->hasMany(ChargingStation::class, 'store_id');
    }
}
