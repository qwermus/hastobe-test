<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Workplan extends Model
{
    use HasFactory;

    /**
     * Convert json plan to object
     * @param $listsValue
     */
    public function getPlanAttribute($value)
    {
        return json_decode($value);
    }
}
