<?php

namespace App\Models;

use App\Traits\PlanableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Tenant
 * @package App\Models
 * @author Volodymyr Lvov
 */
class Tenant extends Model
{
    use HasFactory;
    use PlanableTrait;

    /**
     * @var string [Name to use in Planable Trait]
     */
    private $planType = 'Tenant';

    /**
     * Tenant has many stores
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stores()
    {
        return $this->hasMany(Store::class, 'tenant_id');
    }
}
