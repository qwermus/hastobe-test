<?php

namespace App\Traits;

use App\Models\Workplan;

/**
 * This model can have plans
 * @package App\Traits
 * @author Volodymyr Lvov
 */
trait PlanableTrait
{
    /**
     * Has many workplans through pivot table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function workplans()
    {
        return $this->belongsToMany(
            Workplan::class,
            'workplan_to_parent',
            'parent_id'
        )->whereParentType($this->planType);
    }
}
