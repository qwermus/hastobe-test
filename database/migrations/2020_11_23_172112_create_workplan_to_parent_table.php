<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkplanToParentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workplan_to_parent', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('workplan_id');
            $table->unsignedBigInteger('parent_id');
            $table->enum('parent_type', ['Tenant', 'Store', 'ChargingStation']);
            $table->timestamps();

            $table->foreign('workplan_id')->references('id')->on('workplans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workplan_to_parent');
    }
}
