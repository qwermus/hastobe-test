<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkplansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workplans', function (Blueprint $table) {
            $table->id();
            $table->string('name', 60);
            $table->text('description');
            $table->enum('type', ['Basic','Exception'])->comment('Exception has a priority on basic');
            $table->enum('status', ['Open','Closed'])->comment('It is open or closed in this period');
            $table->enum('interval', ['Weekday','Fixed'])->comment('Weekday is for basic, fixed is for exception');
            $table->json('plan')->comment('Workplan in seconds from the period beginning');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workplans');
    }
}
