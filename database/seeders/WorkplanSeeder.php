<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class WorkplanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workplans')->insert([
            'id' => 1,
            'name' => 'Basic plan for all shops',
            'description' => '',
            'type' => 'Basic',
            'status' => 'Open',
            'interval' => 'Weekday',
            'plan' => json_encode([
                // Monday AM
                ['s' => strtotime('1970-01-01 08:00:00'), 'f' => strtotime('1970-01-01 13:00:00')],
                // Monday PM
                ['s' => strtotime('1970-01-01 14:00:00'), 'f' => strtotime('1970-01-01 20:00:00')],
                // Tuesday AM
                ['s' => strtotime('1970-01-02 08:00:00'), 'f' => strtotime('1970-01-02 13:00:00')],
                // Tuesday PM
                ['s' => strtotime('1970-01-02 14:00:00'), 'f' => strtotime('1970-01-02 20:00:00')],
                // Wednesday AM
                ['s' => strtotime('1970-01-03 08:00:00'), 'f' => strtotime('1970-01-03 13:00:00')],
                // Wednesday PM
                ['s' => strtotime('1970-01-03 14:00:00'), 'f' => strtotime('1970-01-03 20:00:00')],
                // Thursday AM
                ['s' => strtotime('1970-01-04 08:00:00'), 'f' => strtotime('1970-01-04 13:00:00')],
                // Thursday PM
                ['s' => strtotime('1970-01-04 14:00:00'), 'f' => strtotime('1970-01-04 20:00:00')],
                // Friday AM
                ['s' => strtotime('1970-01-05 08:00:00'), 'f' => strtotime('1970-01-05 13:00:00')],
                // Friday PM
                ['s' => strtotime('1970-01-05 14:00:00'), 'f' => strtotime('1970-01-05 20:00:00')],
                // Saturday AM
                ['s' => strtotime('1970-01-06 08:00:00'), 'f' => strtotime('1970-01-06 13:00:00')],
                // Saturday PM
                ['s' => strtotime('1970-01-06 14:00:00'), 'f' => strtotime('1970-01-06 20:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        /**
        This is Exception working hours, they have priority by all stores
        January, 01, December 25 and May 01 are holidays, all stations of this tenant are closed all day long
         */
        DB::table('workplans')->insert([
            'id' => 2,
            'name' => 'Exception: shop closed on holidays',
            'description' => 'This is Exception working hours, they have priority by all stores
January, 01, December 25 and May 01 are holidays, all stations of this tenant are closed all day long
These holidays are every year, so we use flag "repeat" to repeat this action every year',
            'type' => 'Exception',
            'status' => 'Closed',
            'interval' => 'Fixed',
            'plan' => json_encode([
                // Shop doesn't work at New Year
                ['s' => strtotime('2020-01-01 00:00:00'), 'f' => strtotime('2020-01-02 00:00:00')],
                // Shop doesn't work at Christmass
                ['s' => strtotime('2020-12-25 00:00:00'), 'f' => strtotime('2020-12-26 00:00:00')],
                // Shop doesn't work at May, first
                ['s' => strtotime('2020-05-01 00:00:00'), 'f' => strtotime('2020-05-02 00:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        /**
        November, 28, 2020 is 50 years birthday of the tenant, All stations are working from dusk till down
        We use Fixed here because this holiday is only once
         */
        DB::table('workplans')->insert([
            'id' => 3,
            'name' => 'Exception: shops work at 50 years birthday',
            'description' => 'November, 28, 2020 is 50 years birthday of the tenant, All stations are working from dusk till down',
            'type' => 'Exception',
            'status' => 'Open',
            'interval' => 'Fixed',
            'plan' => json_encode([
                ['s' => strtotime('2020-11-28 00:00:00'), 'f' => strtotime('2020-11-29 00:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        DB::table('workplans')->insert([
            'id' => 4,
            'name' => 'Plan for small town stores',
            'description' => 'Mondays, Tuesdays, and Thursdays from 8:00–12:00 and 13:00–17:30,
Wednesdays from 8:00–13:00,
Fridays from 8:00–12:00 and 13:00–20:00,
Saturdays from 10:00–13:00',
            'type' => 'Basic',
            'status' => 'Open',
            'interval' => 'Weekday',
            'plan' => json_encode([
                // Mondays, Tuesdays, and Thursdays from 8:00–12:00 and 13:00–17:30
                // Monday
                ['s' => strtotime('1970-01-01 08:00:00'), 'f' => strtotime('1970-01-01 12:00:00')],
                ['s' => strtotime('1970-01-01 13:00:00'), 'f' => strtotime('1970-01-01 17:30:00')],
                // Tuesdays
                ['s' => strtotime('1970-01-02 08:00:00'), 'f' => strtotime('1970-01-02 12:00:00')],
                ['s' => strtotime('1970-01-02 13:00:00'), 'f' => strtotime('1970-01-02 17:30:00')],
                // Thursdays
                ['s' => strtotime('1970-01-04 08:00:00'), 'f' => strtotime('1970-01-04 12:00:00')],
                ['s' => strtotime('1970-01-04 13:00:00'), 'f' => strtotime('1970-01-04 17:30:00')],
                // Wednesdays from 8:00–13:00
                ['s' => strtotime('1970-01-03 08:00:00'), 'f' => strtotime('1970-01-03 13:00:00')],
                // Fridays from 8:00–12:00 and 13:00–20:00
                ['s' => strtotime('1970-01-05 08:00:00'), 'f' => strtotime('1970-01-05 12:00:00')],
                ['s' => strtotime('1970-01-05 13:00:00'), 'f' => strtotime('1970-01-05 20:00:00')],
                // Saturdays from 10:00–13:00
                ['s' => strtotime('1970-01-06 10:00:00'), 'f' => strtotime('1970-01-06 13:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        DB::table('workplans')->insert([
            'id' => 5,
            'name' => 'Plan for mall stores',
            'description' => 'Mall stores work each day from 10:00 till 22:00',
            'type' => 'Basic',
            'status' => 'Open',
            'interval' => 'Weekday',
            'plan' => json_encode([
                ['s' => strtotime('1970-01-01 10:00:00'), 'f' => strtotime('1970-01-01 22:00:00')],
                ['s' => strtotime('1970-01-02 10:00:00'), 'f' => strtotime('1970-01-02 22:00:00')],
                ['s' => strtotime('1970-01-03 10:00:00'), 'f' => strtotime('1970-01-03 22:00:00')],
                ['s' => strtotime('1970-01-04 10:00:00'), 'f' => strtotime('1970-01-04 22:00:00')],
                ['s' => strtotime('1970-01-05 10:00:00'), 'f' => strtotime('1970-01-05 22:00:00')],
                ['s' => strtotime('1970-01-06 10:00:00'), 'f' => strtotime('1970-01-06 22:00:00')],
                ['s' => strtotime('1970-01-07 10:00:00'), 'f' => strtotime('1970-01-07 22:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        DB::table('workplans')->insert([
            'id' => 6,
            'name' => 'Plan for manager',
            'description' => 'It works 24/7/365 besides exceptions',
            'type' => 'Basic',
            'status' => 'Open',
            'interval' => 'Weekday',
            'plan' => json_encode([
                ['s' => strtotime('1970-01-01 00:00:00'), 'f' => strtotime('1970-01-08 00:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        DB::table('workplans')->insert([
            'id' => 7,
            'name' => 'Store closed for construction',
            'description' => 'This exception for one store',
            'type' => 'Exception',
            'status' => 'Closed',
            'interval' => 'Fixed',
            'plan' => json_encode([
                ['s' => strtotime('2020-06-01 06:00:00'), 'f' => strtotime('2020-06-05 18:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        DB::table('workplans')->insert([
            'id' => 8,
            'name' => 'Station exception one from challenge',
            'description' => 'From 2020-05-20 09:00 through 11:00, a given single charging station is “closed” due to a planned maintenance',
            'type' => 'Exception',
            'status' => 'Closed',
            'interval' => 'Fixed',
            'plan' => json_encode([
                ['s' => strtotime('2020-05-20 09:00:00'), 'f' => strtotime('2020-05-20 11:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        DB::table('workplans')->insert([
            'id' => 9,
            'name' => 'Station exception two from challenge',
            'description' => 'From 2020-06-13 08:00 through 20:00, a given single charging station is “open” due to a promotional event.',
            'type' => 'Exception',
            'status' => 'Open',
            'interval' => 'Fixed',
            'plan' => json_encode([
                ['s' => strtotime('2020-06-13 08:00:00'), 'f' => strtotime('2020-06-13 20:00:00')],
            ]),
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
    }
}
