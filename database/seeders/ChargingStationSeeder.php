<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ChargingStationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('charging_stations')->insert([
            'id' => 1,
            'name' => 'Large city station for customers',
            'store_id' => 1,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('charging_stations')->insert([
            'id' => 2,
            'name' => 'Large city station for employees',
            'store_id' => 1,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('charging_stations')->insert([
            'id' => 3,
            'name' => 'Large city station for manager',
            'store_id' => 1,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('charging_stations')->insert([
            'id' => 4,
            'name' => 'Large city station for employees',
            'store_id' => 1,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('charging_stations')->insert([
            'id' => 5,
            'name' => 'Station with two exceptions',
            'store_id' => 1,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('charging_stations')->insert([
            'id' => 6,
            'name' => 'Small town station for customers',
            'store_id' => 2,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('charging_stations')->insert([
            'id' => 7,
            'name' => 'Small town station for employees',
            'store_id' => 2,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('charging_stations')->insert([
            'id' => 8,
            'name' => 'Small town station for manager',
            'store_id' => 2,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('charging_stations')->insert([
            'id' => 9,
            'name' => 'Mall store station',
            'store_id' => 3,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
    }
}
