<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class WorkhourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 1,
            'parent_id' => 1,
            'parent_type' => 'Tenant',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 2,
            'parent_id' => 1,
            'parent_type' => 'Tenant',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 3,
            'parent_id' => 1,
            'parent_type' => 'Tenant',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 4,
            'parent_id' => 2,
            'parent_type' => 'Store',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 5,
            'parent_id' => 3,
            'parent_type' => 'Store',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);

        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 6,
            'parent_id' => 3,
            'parent_type' => 'ChargingStation',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 6,
            'parent_id' => 8,
            'parent_type' => 'ChargingStation',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 7,
            'parent_id' => 2,
            'parent_type' => 'Store',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 8,
            'parent_id' => 5,
            'parent_type' => 'ChargingStation',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('workplan_to_parent')->insert([
            'workplan_id' => 9,
            'parent_id' => 5,
            'parent_type' => 'ChargingStation',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
    }
}
