<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TenantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tenants')->insert([
            'id' => 1,
            'name' => 'First Tenant',
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
    }
}
