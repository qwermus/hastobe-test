<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stores')->insert([
            'id' => 1,
            'name' => 'Large city store',
            'tenant_id' => 1,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('stores')->insert([
            'id' => 2,
            'name' => 'Small town store',
            'tenant_id' => 1,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
        DB::table('stores')->insert([
            'id' => 3,
            'name' => 'Mall store',
            'tenant_id' => 1,
            'created_at' => NOW(),
            'updated_at' => NOW()
        ]);
    }
}
