## installation

- First of all rename ".env.example" to ".env"
- Create new database and place DB-connect settings in .env 
- Run "**composer install**" inside root folder
- Run migrations "**php artisan migrate**" and seeding "**php artisan db:seed**" to create tables in database. 
- Directories within the **storage** and the **bootstrap/cache** directories should be writable
- Enjoy
