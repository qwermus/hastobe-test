<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Max exception length
    |--------------------------------------------------------------------------
    |
    | Stop "while" cycle after several years
    |
    */

    'exception_max_length' => 60*60*24*365*2,
];
